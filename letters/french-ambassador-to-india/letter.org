Dear Sir,

We are a group of Indian citizens living in France and French citizens of Indian origin. We are writing this letter to you to voice our concern over recent happenings in India, especially with regard to the seeming inability of the Indian government to reassure both its citizens and the world of being able to fulfil its promise to safeguard the life and liberty of each and every one of its citizens, regardless of status, religion, caste or creed, and to ensure that the unalterable basic structure of the Constitution of India, which ensures that India will always remain a secular democracy, is upheld. 

As you are of course well aware, between the 24th and the 26th of February, several districts of the capital, New Delhi, suffered extreme violence and a complete breakdown of law and order. This violence was targeted against the Muslim community of Delhi, as evidenced by the systematic manner in which Muslim homes, businesses and places of worship were marked out and destroyed. Over 40 people have lost their lives, and the injured number more than 200. Entire communities living in these districts have been irreparably traumatised, and many families have fled their homes. These are popular districts, inhabited by the working class and the poor, who as always have borne the burden of religious hatred and fundamentalism.

That violence raged unabated for nearly three days is a severe indictment on the Indian government, who is directly responsible for the deployment and direction of the Delhi police.  







