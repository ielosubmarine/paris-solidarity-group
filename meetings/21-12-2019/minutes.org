#+TITLE: Minutes of the 1st meeting of the Indian Alliance Paris (IAP), 21/12/2019
#+AUTHOR: IAP
* Preface
  This document is a record of the first meeting or gathering of the *Indian Alliance Paris (IAP)* (provisional name), that took place on the *21st of December, 2019* between *14:00* and *16:30* at the *Place du Panthéon (opp. the Bibliothèque Sainte-Geneviève)* and *7 rue Laromiguière, 75005 Paris*.

* Disclaimer
1. As the organisational structure of the IAP and the format of discussions, proposals and decisions undertaken during its meetings have yet to be clearly defined, this document has been prepared as a summary rather than as a faithful recording of minutes of the gathering.
2. In light of (1.), any decisions purported by this document to have been taken during the meeting have not been attributed to any individual member(s), and are rather to be understood as having been taken with the unanimous accord of all members.
3. In light of (1.), the structure of this document does not strictly follow the chronological sequence of points raised during the meeting and of subsequent discussions. This is due to the haphazard manner in which the subject under discussion during the meeting oscillated between items on the agenda before any conclusive decisions were taken. A more organised structuring of subsequent meetings---with a chair, a sequential agenda of items, recorded votes, etc. would facilitate the taking of detailed minutes and render them more transparent and attestable.

* Decisions taken
** Main Protest (4th Jan)
  There will be a protest/demonstration organised by the IAP (henceforth referred to as ``the protest''). 

*** Date
   The protest will take place on the *4th of January, 2020*.

*** Location 
   The protest will begin at the *Esplanade du Trocadero* (``Trocadero'') and will recongregate at the *Indian Embassy (Paris), 13 Rue Alfred Dehodencq, 75116 Paris* (``the Embassy'').

*** Time
   - The gathering will assemble at Trocadero from *11:30 AM* and the first session of the protest will begin at *12:00 PM* and end at *2:30 PM*.
   - The gathering will recongregate at the Embassy at *3:00 PM* and the second session of the protest will begin at *3:00 PM* and end at *5:00 PM*.

*** Police permission 
    - The protest will take place subject to permission granted by the Police Nationale.
    - One or more individual members (``organisers'') will have to take responsibility for the protest.
    - A committee consisting of the following members will contact the Préfecture de Police in Paris *as soon as possible* (next working day if possible) for information about obtaining permission for the protest: Abhishek, Chaitanya, Gurtegh, Ilu, Sreyash, Sudarshan.

*** Attendees
    - The IAP (certain individual members or ``organisers'') will be responsible for the attendees at the protest.
    - One may attend the protest *by invitation only* and such invitation will be extended to *individuals only* and not to larger groups/communities.
    - The total number of attendees will be restricted to a maximum of *100 people* (due to constraints of police permission). 
    - It is important that people attend the protest of their own volition and conviction.  

*** Roles
   - There will be different roles occupied by members of the IAP during the protest (such as organisers, media coverage, logistics, etc.), but the exact definition and scope of these roles have not yet been decided.

*** Further remarks

**** Police permission
     - Details about getting police permission may be found on the [[https://www.prefecturedepolice.interieur.gouv.fr/Demarches/Professionnel/Securite-et-accessibilite-des-batiments/Manifestation-sur-la-voie-publique-ou-tout-espace-ouvert-au-public][website]] of the Préfecture de Police.
 
**** Ideas for the protest 
     - ``Big-ass'' Indian flag.
     - Demonstration against ``can be identified by their clothes''.
     - Modi face cut-outs.
     - ``Grave of the Indian constitution''.
     - Pamphlets/posters to be distributed.
     - Reading of the preamble, letters of solidarity, speeches, poems.

**** Dispersal and Recongregation 
     - At 2:30 PM on the day of the protest, members have informally agreed to walk from the first location (Trocadero) to the second location (the Embassy). Attendees should be invited and encouraged to join the walk, however, this *will not* be considered part of the protest (for reasons of police permission), and the organisers of the protest *will not* be responsible for the actions of attendees during this walk. 

** Regular Saturday protests
   It was decided that regular protests by individual members every Saturday at Trocadero or the Embassy were to be encouraged.

** Statement of solidarity 
   The IAP has drafted a statement of solidarity, in English and in French, against the NRC and the CAA. This statement is to be considered *separate* from the protest in its drafting, signatories and circulation.

*** Release
    The statement along with its signatures will be released in conjunction with the protest, that is on the *4th of January, 2020*.

*** Signatories
    - The statement should be circulated widely, among individuals (friends, colleagues) and groups (organisations, associations, mailing lists). In particular, the restrictions that apply to invitations to the protest *do not apply* to the circulation of the statement.
    - Certain members have put forth the possibility of circulating the statement among larger groups (e.g. La France Insoumise), which is to be encouraged.

** Facebook group
   There will be an IAP group on Facebook.

*** Membership
    A decision needs to be taken on how membership to the group should be granted. The need to prevent antisocial/malicious elements from infiltrating the group was raised and agreed upon.
    - Ideas put forth for a possible vetting procedure:
      + Invitation by a member.
      + Verification by 3 members.
    - It was suggested that membership be open to the public, with removal of members from the group as and when required. 

** Structure of the IAP

*** Subcommittees 
    - It was decided that the IAP should have subcommittees to deal with individual tasks.

    - The following subcommittees with individual responsibilities have been created. Members are asked to volunteer for inclusion in a maximum of two subcommittees. This list is included here /verbatim/ from an email sent to the mailing list.
     1. *Logistics:* To organise the logistics of events, like police permission in case of a protest etc.
     2. *Association* To manage membership, and the functioning of whatsapp, google, and facebook groups.
     3. *Design:* To design posters, banners, placards for events.
     4. *Outreach:* To communicate with the Indian diaspora in Paris. The committee needs to ensure it does not simply limit itself to connecting to "core" or in Edward Said's phrase the "metropolitan" diaspora, but also the "peripheral" diaspora. For example, it needs to reach out to diasporic northeastern communities, diasporic Kashmiri communities, diasporic Tamil refugee communities, etc.
     5. *Media:* To contact journalist, news agencies, and write press statements, and manage material to be distributed to the media.
     6. *Photography:* To photograph and to take videos of events.
     7. *Social Media:* To plan and synchronize posting event or other materials like protest photos, statements, etc. on various social media platform.
     8. *Ideology:* To initiate political discussion and political education, curate reading lists, and ensure the political training gets reflected in practice. This subcommittee will also oversee writing statements.
     9. *Fundraising:* To raise funds from us, and to decide venues where funds are to be used for. Fundraising will support medical and legal expenditures for actual on ground protesters in India, as well as independent media outlets which do their best to bring out the real news. Care needs to be taken to divert the funds without being myopic to the peripheral. For example, Jamia gets more limelight than AMU, which in turn gets more limelight than colleges in the northeast. Delhi gets more limelight than Mangalore. Wire and Scroll get more limelight than the vernacular press. Fundraising subcommittee needs to ensure that the funds get allocated according to the need.
     10. *Legal:* To ensure the legal protection of our members in Paris, France. Only those with actual legal expertise volunteer for this.




* Closing Remarks 
  The IAP thanked member *Ilu Ben* for her generous hospitality in proposing her home as the location where the meeting recongregated (due to inclement weather). 

   

 
