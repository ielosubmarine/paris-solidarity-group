\contentsline {section}{\numberline {1}Preface}{2}{section.1}
\contentsline {section}{\numberline {2}Disclaimer}{2}{section.2}
\contentsline {section}{\numberline {3}Decisions taken}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Main Protest (4th Jan)}{2}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Date}{2}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Location}{3}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}Time}{3}{subsubsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.4}Police permission}{3}{subsubsection.3.1.4}
\contentsline {subsubsection}{\numberline {3.1.5}Attendees}{3}{subsubsection.3.1.5}
\contentsline {subsubsection}{\numberline {3.1.6}Roles}{4}{subsubsection.3.1.6}
\contentsline {subsubsection}{\numberline {3.1.7}Further remarks}{4}{subsubsection.3.1.7}
\contentsline {subsection}{\numberline {3.2}Regular Saturday protests}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Statement of solidarity}{4}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Release}{5}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Signatories}{5}{subsubsection.3.3.2}
\contentsline {subsection}{\numberline {3.4}Facebook group}{5}{subsection.3.4}
\contentsline {subsubsection}{\numberline {3.4.1}Membership}{5}{subsubsection.3.4.1}
\contentsline {subsection}{\numberline {3.5}Structure of the IAP}{5}{subsection.3.5}
\contentsline {subsubsection}{\numberline {3.5.1}Subcommittees}{5}{subsubsection.3.5.1}
\contentsline {section}{\numberline {4}Closing Remarks}{7}{section.4}
